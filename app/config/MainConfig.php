<?php

define("__URI__", $_SERVER['REQUEST_URI']);
define("__ROOT__", $_SERVER['DOCUMENT_ROOT']);
define("__APP__", $_SERVER['DOCUMENT_ROOT']."/app/");
define("__CONFIG__", $_SERVER['DOCUMENT_ROOT']."/app/config/");
define("__CONTROLLERS__", $_SERVER['DOCUMENT_ROOT']."/app/controllers/");
define("__MODELS__", $_SERVER['DOCUMENT_ROOT']."/app/models/");
define("__PLUGINS__", $_SERVER['DOCUMENT_ROOT']."/app/plugins/");