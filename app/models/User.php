<?php

class User extends dbConnection
{
    /**
     * @return array
     */
    public function getAllUsersInfo()
    {
        $connection = $this->connectDB();
        $query = "SELECT us.*, ps.name, sl.amount FROM positions AS ps 
                    LEFT JOIN users AS us ON ps.id=us.position_id 
                    LEFT JOIN salary AS sl ON us.id=sl.user_id
                    LIMIT 0, 5";
        $object = $connection->query($query);

        $result = $object->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param $counter
     * @return false|string
     */
    public static function getCounterUsersInfoJson($counter)
    {
        $connection = self::connectDB();
        $query = "SELECT us.*, ps.name, sl.amount FROM positions AS ps 
                    LEFT JOIN users AS us ON ps.id=us.position_id 
                    LEFT JOIN salary AS sl ON us.id=sl.user_id
                    LIMIT {$counter}, 5";
        $prepare = $connection->prepare($query);
        $exec = $prepare->execute();
        if( $exec )
        {
            $result = $prepare->fetchAll(PDO::FETCH_ASSOC);
            return json_encode($result);
        }
    }
}