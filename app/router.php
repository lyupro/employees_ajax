<?php

$getUri = parse_url(__URI__, PHP_URL_PATH);
$getUri = trim($getUri,"/");

if( !empty($getUri)){
    if(array_key_exists($getUri, $routes)){
        $getController = $routes[$getUri];
    }
}else{
    $getController = $routes['main'];
}

$array = explode("|", $getController);
$file = $array[0];
$class = $array[0];
$method = $array[1];

$getController = __CONTROLLERS__.$file.".php";
//echo $getController; // DEBUG

try{
    if( !file_exists($getController)){
        throw new Exception('Controller is not exists!');
    }else{
        require_once __CONTROLLERS__.$file.".php";
    }
}catch (Exception $exception){
    $except = $exception->getMessage()."<br>";
}

try{
    if( !class_exists($class)){
        throw new Exception('Class is not exists!');
    }else{
        require_once __CONTROLLERS__.$file.".php";
    }
}catch (Exception $exception){
    $except .= $exception->getMessage();
}

if(isset($except)){
    $getTwig = Template::twig();
    $getMenu = $getTwig->render("menu.html");
    $html = $getTwig->render("header.html", ["menu"=>$getMenu]);
    $html .= $getTwig->render("index.html", ["exception"=>$except]);
    $html .= $getTwig->render("footer.html");
    return;
}


if( class_exists($class) ){
    $getTwig = Template::twig();
    $getMenu = $getTwig->render("menu.html");
    $html = $getTwig->render("header.html", ["menu"=>$getMenu]);

    $newObj = new $class;
    $html .= $newObj->$method();
    $html .= $getTwig->render("footer.html");
}