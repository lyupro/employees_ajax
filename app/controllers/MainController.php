<?php

class MainController extends Template
{
    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index()
    {
        $getTwig = self::twig();
        return $getTwig->render("index.html", ["content" => "Hello World!"]);
    }
}