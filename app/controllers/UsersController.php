<?php

require_once __MODELS__."User.php";

class UsersController extends Template
{
    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index()
    {
        $model = new User();
        $array = $model->getAllUsersInfo();

        foreach ($array as $user) {
            $getTable .= "<tr>
                    <td>$user[middle_name] $user[first_name] $user[last_name]</td>
                    <td>$user[name]</td>
                    <td>$user[amount]$</td>
                    <td>$user[created_at]</td>
                    </tr>
                   ";
        }

        $button = "<button class='load' data-row='5'>Load More...</button>";
        $getTwig = self::twig();
        return $getTwig->render("users.html", ["table"=>$getTable, "button"=>$button]);
    }
}

