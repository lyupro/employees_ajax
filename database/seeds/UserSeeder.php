<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{

    private $positions = [
        1,
        2, 2,
        3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
    ];

    private $parents = [
        0,
        1, 1,
        2, 2, 3, 3,
        4, 4, 4, 4, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7
    ];

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for( $i = 0; $i < 30; $i++){

            $data[] = [
                'first_name'      => $faker->firstName,
                'last_name'      => $faker->lastName,
                'middle_name'      => $faker->title,
                //'parent_id'      => $faker->numberBetween(1, 5),
                //'position_id'      => $faker->numberBetween(2, 5),
                'parent_id'      => $this->parents[$i],
                'position_id'      => $this->positions[$i],
                'created_at'       => date('Y-m-d H:i:s'),
                'updated_at'       => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('users')->insert($data)->save();
    }
}
